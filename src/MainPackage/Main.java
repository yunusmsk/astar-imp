package MainPackage;

import java.text.DecimalFormat;
import java.util.ArrayList;

import FacilityInformation.*;

public class Main {
	private static OrderGenerator gen = new OrderGenerator();
	private static DistanceMatrix matrix = new DistanceMatrix();
	private static AStar algorithm = new AStar();

	private static ArrayList<Node> nodes = matrix.readDistances();
	private static ArrayList<Order> orders = gen.getOrders(nodes);

	static double pathLengthEmpty = 0.0;
	static double pathLengthFull = 0.0;
	
	public static void main(String[] args) {
		matrix.print();
		// �bergebe dem Netzwerk die Menge aller Knoten
		algorithm.setNodes(nodes);
		
		startSingleOrder();
		startOrdersFiFo();
		startOrdersSorted();
	}
	
	
	/**
	 * Aufgabenteil b)
	 */
	private static void startSingleOrder() {
		System.out.println("Single order:");
		Order singleOrder = gen.getSingleOrder(nodes);
		System.out.println(singleOrder.getStart().getName() + " -> " + singleOrder.getGoal().getName());
		
		/**
		 * Implementieren Sie hier die Bearbeitung des Transportauftrags A -> J.
		 * Den Start- und End-Knoten erhalten Sie �ber die Aufrufe:
		 * singleOrder.getStart();
		 * singleOrder.getGoal();
		 */
	}
	
	
	/**
	 * Aufgabenteil c)
	 */
	private static void startOrdersFiFo() {
		setPathLengthEmpty(0.0);
		setPathLengthFull(0.0);
		
		System.out.println("Complete order list:");
		
		for (Order order : orders) {
			printOrder(order, true);
		}
		
		System.out.println("Optimal routes:");		
		
		/**
		 * Implementieren Sie hier die Auftragsabarbeitung
		 * in der Reihenfolge ihrer Erzeugung
		 */
		
		printResult();
	}
	
	
	/**
	 * Aufgabenteil d)
	 */
	private static void startOrdersSorted() {
		setPathLengthEmpty(0.0);
		setPathLengthFull(0.0);
		
		/**
		 * Implementieren Sie hier die Auftragsabarbeitung
		 * in optimierter Reihenfolge
		 */
	}
	
	
	/**
	 * Ausgabe des Ergebnisses der Berechnung auf der Konsole
	 */
	private static void printResult() {
		System.out.println();
		System.out.println("Overall path length = " + (getPathLengthEmpty() + getPathLengthFull()));
		
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("Percentage of empty runs = " + df.format((getPathLengthEmpty() / (getPathLengthEmpty() + getPathLengthFull()))*100) + " %");
		
		System.out.println();
		System.out.println("----------------------------------------------------");
		System.out.println();
	}
	
	
	/**
	 * Ausgabe der Auftragsinformationen auf der Konsole
	 * @param order
	 * @param all
	 */
	private static void printOrder(Order order, boolean all) {
		if (all) {
			System.out.println(order.getStart().getName()
					+ " (" + order.getStart().getCoordinate().getX()
					+ "," + order.getStart().getCoordinate().getY()
					+ ") -> " + order.getGoal().getName()
					+ " (" + order.getGoal().getCoordinate().getX()
					+ "," + order.getGoal().getCoordinate().getY() + ")");			
		}
		
		else {
			System.out.print(order.getStart().getName()
					+ " -> " + order.getGoal().getName()
					+ ": ");
		}
	}
	
	public static double getPathLengthEmpty() {
		return pathLengthEmpty;
	}


	public static void setPathLengthEmpty(double pathLengthEmpty) {
		Main.pathLengthEmpty = pathLengthEmpty;
	}


	public static double getPathLengthFull() {
		return pathLengthFull;
	}


	public static void setPathLengthFull(double pathLengthFull) {
		Main.pathLengthFull = pathLengthFull;
	}
}
