package MainPackage;

import java.util.ArrayList;

import FacilityInformation.Node;
import FacilityInformation.Path;

public class AStar {

	/**
	 * Implementieren Sie hier den A*-Algorithmus
	 */
	
	/** Alle Knoten des Netzwerks */
	private ArrayList<Node> nodes;

	
	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}

}
