package MainPackage;

import java.util.ArrayList;

import FacilityInformation.Node;

/**
 * Objekte dieser Klasse k�nnen dazu genutzt werden, die Ergebnisse des
 * A*-Algorithmus an die Hauptklasse zu �bergeben.
 */
public class Result {

	private ArrayList<Node> pathList;
	private double pathLength;
	
	public Result(ArrayList<Node> pathList, double pathLength) {
		setPathList(pathList);
		setPathLength(pathLength);
	}
	
	public Result() {
		setPathList(new ArrayList<Node>());
		setPathLength(0.0);
	}
		
	public ArrayList<Node> getPathList() {
		return pathList;
	}
	
	public void setPathList(ArrayList<Node> pathList) {
		this.pathList = pathList;
	}
	
	public double getPathLength() {
		return pathLength;
	}
	
	public void setPathLength(double pathLength) {
		this.pathLength = pathLength;
	}
}
